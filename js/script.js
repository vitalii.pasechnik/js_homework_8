"use strict";


/*1. Опишіть своїми словами що таке Document Object Model (DOM)

   Document Object Model - это концепция, в которой HTML-страница представляется в виде дерева элементов, каждый из которых является объектом. 
   Блягодаря этому мы можем находить любые элементы на странице и управлять ими, используя объектную модель поведения.
*/

/* 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    С помощью innerText HTML-елементам можно передать только строковые значения, которые воспринимаются только как текст, 
    а через innerHTML в виде строки можно передавать HTML-разметку, которая превратится в нод-элементы и коррректно добавится в качестве дочерних элементов объекта, 
    у которого мы вызываем свойство innerHTML.  
*/

/* 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

   К єлементам на странице можно обратьтся разніми способами, например: 
        - document.getElementById('id');
        - document.getElementsByClassName('class');
        - document.getElementsByTagName('tag');
        - document.querySelector('selector');
        - document.querySelectorAll('selector');
   Можно также обращатся к соседним, родительским или дочерним элементам любого элемента:
        - el.childNodes; 
        - el.firstChild; 
        - el.lastChild; 
        - el.parentNode; 
        - el.nextSibling;
        - el.previousSibling. 
    Лучшего способа быть не может, все зависит от ситуации, хотя более универсальный способ - это querySelector;
*/

// --- 1 ---
const paragraphNodes = document.querySelectorAll('p');
paragraphNodes.forEach(p => p.style.backgroundColor = '#ff0000');

// --- 2 ---
const optionsList = document.getElementById('optionsList');

const optionsListParent = optionsList.parentNode;
// const optionsListParent = optionsList.parentElement;
const optionsListChildren = optionsList.childNodes;

console.log('optionsList: \n', optionsList);
console.log('optionsList parent: \n', optionsListParent);
console.log('optionsList children: \n', optionsListChildren);

// --- 3 ---
const paragraph = document.querySelector('#testParagraph');
console.log('testParagraph: \n', paragraph);
paragraph.textContent = 'This is a paragraph';

// --- 4, 5 ---
const header = document.querySelector('.main-header');
const headerChildren = header.children;

console.log('main-header: \n', header);
console.log('main-header children: \n', ...headerChildren);

Array.from(headerChildren).forEach(el => el.classList.add('nav-item'));

// --- 6 ---
const sectionElements = document.querySelectorAll('.section-title');

sectionElements.forEach(el => el.classList.remove('section-title'))
console.log('section-title elements: \n', sectionElements);





